FROM adoptopenjdk/openjdk12:alpine as builder

RUN java --list-modules

RUN jlink \
    --module-path ../jmods \
    --add-modules java.base,java.logging,java.sql,java.management,jdk.management.agent,java.instrument,java.security.jgss,jdk.zipfs \
    --verbose \
    --strip-debug \
    --compress 2 \
    --no-header-files \
    --no-man-pages \
    --output /opt/jre-minimal

RUN /opt/jre-minimal/bin/java -version

FROM alpine:latest

RUN apk --update add --no-cache ca-certificates curl openssl binutils xz \
    && GLIBC_VER="2.28-r0" \
    && ALPINE_GLIBC_REPO="https://github.com/sgerrand/alpine-pkg-glibc/releases/download" \
    && curl -Ls ${ALPINE_GLIBC_REPO}/${GLIBC_VER}/glibc-${GLIBC_VER}.apk > /tmp/${GLIBC_VER}.apk \
    && apk add --allow-untrusted /tmp/${GLIBC_VER}.apk \
    && curl -Ls https://www.archlinux.org/packages/core/x86_64/gcc-libs/download > /tmp/gcc-libs.tar.xz \
    && mkdir /tmp/gcc \
    && tar -xf /tmp/gcc-libs.tar.xz -C /tmp/gcc \
    && mv /tmp/gcc/usr/lib/libgcc* /tmp/gcc/usr/lib/libstdc++* /usr/glibc-compat/lib \
    && strip /usr/glibc-compat/lib/libgcc_s.so.* /usr/glibc-compat/lib/libstdc++.so* \
    && curl -Ls https://www.archlinux.org/packages/core/x86_64/zlib/download > /tmp/libz.tar.xz \
    && mkdir /tmp/libz \
    && tar -xf /tmp/libz.tar.xz -C /tmp/libz \
    && mv /tmp/libz/usr/lib/libz.so* /usr/glibc-compat/lib \
    && apk del ca-certificates curl openssl binutils xz \
    && rm -rf /tmp/${GLIBC_VER}.apk /tmp/gcc /tmp/gcc-libs.tar.xz /tmp/libz /tmp/libz.tar.xz /var/cache/apk/*

COPY --from=builder /opt/jre-minimal /opt/jre-minimal

ENV PATH="/opt/jre-minimal/bin:${PATH}"

ARG JVM_OPTS
ENV JVM_OPTS=${JVM_OPTS}

COPY build/libs/*.jar instajobzz-users.jar
EXPOSE 8080

CMD java -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar instajobzz-users.jar