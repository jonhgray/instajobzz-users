package me.instajobzz.api.users.mail;

public interface MailService {

    void sendInvitationEmail(String to, String token);
}
