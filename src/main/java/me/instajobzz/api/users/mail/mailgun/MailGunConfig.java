package me.instajobzz.api.users.mail.mailgun;

import io.micronaut.context.annotation.ConfigurationProperties;
import java.net.URL;

@ConfigurationProperties("instajobzz.mail.mailgun")
class MailGunConfig {

    String authorization;

    String domain;

    URL baseUrl;

    boolean testMode;
}
