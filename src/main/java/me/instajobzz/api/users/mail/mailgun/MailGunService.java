package me.instajobzz.api.users.mail.mailgun;

import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import lombok.extern.slf4j.Slf4j;
import me.instajobzz.api.users.mail.MailService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.Map;

@Singleton
@Slf4j
public class MailGunService implements MailService {

    private final String invitationEmailAddress;
    private final String authorization;
    private final MailGunConfig config;

    private final HttpClient client;

    public MailGunService(@Property(name = "instajobzz.mail.invitataion-email-address") String invitationEmailAddress,
                          MailGunConfig config) {

        this.client = HttpClient.create(config.baseUrl);
        this.config = config;
        this.invitationEmailAddress = invitationEmailAddress;
        this.authorization = "Basic " + Base64.getEncoder().encodeToString(config.authorization.getBytes());
    }

    @Override
    public void sendInvitationEmail(String to, String token) {
        if (config.testMode) {
            log.info("Sending in test mode");
        }

        var data = Map
                .of("from", invitationEmailAddress,
                        "to", to,
                        "subject", "Добро пожаловать на InstaJobzz",
                        "template", "instajobzz-invitation",
                        "v:token", token,
                        "o:testmode", config.testMode ? "true": "false");

        var request = HttpRequest.POST("/v3/"+ config.domain + "/messages", data)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .header("Authorization", authorization);

        var response = client.toBlocking().exchange(request, String.class);

        log.info("Got response from MailGun - {}, {}", response.status(), response.body());
    }
}
