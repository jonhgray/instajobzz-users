package me.instajobzz.api.users.authentication;

import com.google.common.collect.ImmutableMap;
import io.micronaut.security.authentication.*;
import io.micronaut.spring.tx.annotation.Transactional;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import me.instajobzz.api.users.authentication.repository.AuthenticationRepository;
import me.instajobzz.api.users.domain.UserNotFoundException;
import org.reactivestreams.Publisher;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Singleton;
import java.util.Collections;

@Singleton
@Slf4j
public class AuthenticationProviderUserPassword implements AuthenticationProvider  {

    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    private final AuthenticationRepository authenticationRepository;

    public AuthenticationProviderUserPassword(AuthenticationRepository authenticationRepository) {
        this.authenticationRepository = authenticationRepository;
    }

    @Override
    @Transactional
    public Publisher<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {
        try {
            var user = authenticationRepository.findOne((String) authenticationRequest.getIdentity());
            if (user != null) {
                if (encoder.matches((String) authenticationRequest.getSecret(), user.getPassword())) {
                    return Flowable.just(new UserDetails(user.getName(),
                            Collections.emptyList(), ImmutableMap.of("name", user.getName(),
                            "surname", user.getSurname(), "email", user.getEmail())));
                }
                else {
                    log.info("Проверка пароля для пользователя {} не пройдена", user.getId());
                    return Flowable.just(new AuthenticationFailed(AuthenticationFailureReason.CREDENTIALS_DO_NOT_MATCH));
                }
            } else {
                log.error("Ошибка получения пользователя по логину {}", authenticationRequest.getIdentity());
                return Flowable.just(new AuthenticationFailed());
            }
        } catch (UserNotFoundException e) {
            log.info("Пользотваель с логином {} не найден", authenticationRequest.getIdentity());
            return Flowable.just(new AuthenticationFailed(AuthenticationFailureReason.USER_NOT_FOUND));
        }
    }
}