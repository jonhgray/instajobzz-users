package me.instajobzz.api.users.authentication.repository;

import me.instajobzz.api.users.domain.Operator;
import me.instajobzz.api.users.domain.UserNotFoundException;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

@Singleton
public class AuthenticationRepository {

    @PersistenceContext
    private EntityManager em;

    public Operator findOne(String login) throws UserNotFoundException {
        var query = em.createQuery("SELECT u FROM UserInfo AS u WHERE u.email = :login", Operator.class);

        query.setParameter("login", login);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            throw new UserNotFoundException(login);
        } catch (NonUniqueResultException e) {
            return null;
        }
    }
}
