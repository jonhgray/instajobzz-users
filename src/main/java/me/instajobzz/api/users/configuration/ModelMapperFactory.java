package me.instajobzz.api.users.configuration;

import io.micronaut.context.annotation.Factory;
import me.instajobzz.api.users.domain.Operator;
import me.instajobzz.api.users.domain.dto.OperatorRegistrationDto;
import org.modelmapper.ModelMapper;

import javax.inject.Singleton;

@Factory
public class ModelMapperFactory {

    @Singleton
    ModelMapper modelMapper() {
        var mapper = new ModelMapper();

        var operatorRegistrationTypeMap =  mapper.createTypeMap(OperatorRegistrationDto.class, Operator.class);
        operatorRegistrationTypeMap.addMappings(m -> m.skip(Operator::setPassword));

        return mapper;
    }
}
