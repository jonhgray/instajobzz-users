package me.instajobzz.api.users.invitation.service;

import com.google.common.io.BaseEncoding;
import io.micronaut.context.annotation.Property;
import io.micronaut.spring.tx.annotation.Transactional;
import lombok.extern.slf4j.Slf4j;
import me.instajobzz.api.users.domain.Invitation;
import me.instajobzz.api.users.domain.dto.InvitataionDto;
import me.instajobzz.api.users.mail.MailService;
import me.instajobzz.api.users.invitation.service.repository.InvitationRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Singleton;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Singleton
public class InvitationServiceImpl implements InvitationService {

    private final Duration invitataionTTL;

    private final Random random = getSecureRandom();

    private final InvitationRepository invitationRepository;

    private final MailService mailService;

    private final ModelMapper modelMapper;

    public InvitationServiceImpl(InvitationRepository invitationRepository,
                                 MailService mailService,
                                 ModelMapper modelMapper,
                                 @Property(name = "instajobzz.users.invitation-ttl") Duration invitataionTTL) {
        this.invitationRepository = invitationRepository;
        this.modelMapper = modelMapper;
        this.mailService = mailService;
        this.invitataionTTL = invitataionTTL;
    }

    @Override
    @Transactional
    public void sendInvite(InvitataionDto invitataionDto) {
        var invite = modelMapper.map(invitataionDto, Invitation.class);

        var id = UUID.randomUUID().toString();
        invite.setId(id);
        invite.setToken(generateToken());
        invite.setExpiresAt(LocalDateTime.now().plus(invitataionTTL));

        if (invitationRepository.save(invite)) {
            mailService.sendInvitationEmail(invite.getEmail(), invite.getToken());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public void validateInvite(String token) throws InvitationNotFoundException {
        invitationRepository
                .findOneByTokenWithExpiredTimeLessThan(token, LocalDateTime.now())
                .orElseThrow(InvitationNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public void validateInvite(String email, String token) throws InvitationNotFoundException {
        invitationRepository
                .findOneByTokenAndEmailWithExpiredTimeLessThan(email, token, LocalDateTime.now())
                .orElseThrow(InvitationNotFoundException::new);
    }

    private static Random getSecureRandom() {
        try {
            return SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            log.warn("Невозможно инстанцировать генератор сильных случайных чисел", e);
            return new SecureRandom();
        }
    }

    private String generateToken() {
        final byte[] buffer = new byte[512];
        random.nextBytes(buffer);
        return BaseEncoding.base64Url().omitPadding().encode(buffer);
    }
}
