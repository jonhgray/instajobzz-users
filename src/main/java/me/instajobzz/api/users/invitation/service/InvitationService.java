package me.instajobzz.api.users.invitation.service;

import me.instajobzz.api.users.domain.dto.InvitataionDto;

public interface InvitationService {

    void sendInvite(InvitataionDto invitataionDto);

    void validateInvite(String token) throws InvitationNotFoundException;

    void validateInvite(String email, String token) throws InvitationNotFoundException;
}
