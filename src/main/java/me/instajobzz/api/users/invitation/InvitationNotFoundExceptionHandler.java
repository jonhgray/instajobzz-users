package me.instajobzz.api.users.invitation;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import me.instajobzz.api.users.invitation.service.InvitationNotFoundException;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

import javax.inject.Singleton;

@Produces
@Singleton
@Requires(classes = {InvitationNotFoundException.class, Problem.class, ExceptionHandler.class})
public class InvitationNotFoundExceptionHandler implements ExceptionHandler<InvitationNotFoundException, HttpResponse<Problem>> {

    @Override
    public HttpResponse<Problem> handle(HttpRequest request, InvitationNotFoundException exception) {
        var problem = Problem.builder().withStatus(Status.NOT_FOUND).withTitle("Приглашение не найдено").build();

        return HttpResponse.notFound(problem);
    }
}
