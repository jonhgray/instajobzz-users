package me.instajobzz.api.users.invitation;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import me.instajobzz.api.users.domain.dto.InvitataionDto;
import me.instajobzz.api.users.invitation.service.InvitationNotFoundException;
import me.instajobzz.api.users.invitation.service.InvitationService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Validated
@Secured(SecurityRule.IS_ANONYMOUS)
@Controller("/invitation")
public class InvitationController {

    private final InvitationService invitationService;

    public InvitationController(InvitationService invitationService) {
        this.invitationService = invitationService;
    }

    @Post()
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON + "+problem"})
    public HttpResponse invite(@Valid @Body InvitataionDto invitataionDto) {
        invitationService.sendInvite(invitataionDto);

        return HttpResponse.accepted();
    }

    @Get("/{token}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON + "+problem"})
    public HttpResponse invite(@NotEmpty @PathVariable String token) throws InvitationNotFoundException {
        invitationService.validateInvite(token);

        return HttpResponse.ok();
    }
}
