package me.instajobzz.api.users.invitation.service.repository;

import io.micronaut.spring.tx.annotation.Transactional;
import me.instajobzz.api.users.domain.Invitation;
import org.springframework.transaction.annotation.Propagation;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Optional;

@Singleton
@Transactional(propagation = Propagation.MANDATORY)
public class InvitationRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public boolean save(Invitation invitation) {
        var query = entityManager.createNativeQuery("INSERT INTO invitation(id, email, token, expires_at, aud_ts_created, aud_ts_last_modified) " +
                "VALUES (:id, :email, :token, :expires_at, :aud_ts_created, :aud_ts_updated) ON CONFLICT DO NOTHING RETURNING id");

        query.setParameter("id", invitation.getId());
        query.setParameter("email", invitation.getEmail());
        query.setParameter("token", invitation.getToken());
        query.setParameter("expires_at", invitation.getExpiresAt());
        query.setParameter("aud_ts_created", LocalDateTime.now());
        query.setParameter("aud_ts_updated", LocalDateTime.now());

        return query.getResultStream().findFirst().isPresent();
    }

    public Optional<Invitation> findOneByTokenWithExpiredTimeLessThan(String token, LocalDateTime time) {
        var query = entityManager.createQuery("SELECT i FROM Invitation AS i " +
                "WHERE i.token = :token AND i.expiresAt > :time", Invitation.class);

        query.setParameter("token", token);
        query.setParameter("time", time);

        return query.getResultStream().findFirst();
    }

    public Optional<Invitation> findOneByTokenAndEmailWithExpiredTimeLessThan(String email, String token, LocalDateTime time) {
        var query = entityManager.createQuery("SELECT i FROM Invitation AS i " +
                "WHERE i.token = :token AND i.email = :email AND i.expiresAt > :time", Invitation.class);

        query.setParameter("token", token);
        query.setParameter("time", time);
        query.setParameter("email", email);

        return query.getResultStream().findFirst();
    }
}
