package me.instajobzz.api.users.registration;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import me.instajobzz.api.users.domain.dto.OperatorRegistrationDto;
import me.instajobzz.api.users.invitation.service.InvitationNotFoundException;
import me.instajobzz.api.users.registration.service.RegistrationService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.net.URI;

@Validated
@Secured(SecurityRule.IS_ANONYMOUS)
@Controller("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Post("/{invitationToken}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON + "+problem"})
    public HttpResponse registerUser(@Valid @Body OperatorRegistrationDto registrationInfoDto,
                                     @NotEmpty @PathVariable String invitationToken) throws InvitationNotFoundException {
        var id = registrationService.registerUser(registrationInfoDto, invitationToken);

        return HttpResponse
                .created(id)
                .headers(h -> h.location(location(id.getId())));
    }

    private URI location(String id) {
        return URI.create("/" + id);
    }
}
