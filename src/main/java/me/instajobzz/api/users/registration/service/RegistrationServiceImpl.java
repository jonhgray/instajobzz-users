package me.instajobzz.api.users.registration.service;

import io.micronaut.spring.tx.annotation.Transactional;
import me.instajobzz.api.users.domain.Operator;
import me.instajobzz.api.users.domain.dto.Id;
import me.instajobzz.api.users.domain.dto.OperatorRegistrationDto;
import me.instajobzz.api.users.invitation.service.InvitationNotFoundException;
import me.instajobzz.api.users.invitation.service.InvitationService;
import me.instajobzz.api.users.registration.repository.RegistrationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
public class RegistrationServiceImpl implements RegistrationService {

    private final PasswordEncoder pWdEncoder = new BCryptPasswordEncoder();

    private final InvitationService invitataionService;

    private final RegistrationRepository repository;
    private final ModelMapper modelMapper;

    public RegistrationServiceImpl(InvitationService invitationService,
                                   RegistrationRepository repository,
                                   ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
        this.invitataionService = invitationService;
    }

    @Override
    @Transactional
    public Id registerUser(OperatorRegistrationDto operatorRegistrationDto, String invitationToken) throws InvitationNotFoundException {
        invitataionService.validateInvite(operatorRegistrationDto.getEmail(), invitationToken);

        var operator = modelMapper.map(operatorRegistrationDto, Operator.class);
        operator.setId(UUID.randomUUID().toString());
        operator.setPassword(pWdEncoder.encode(String.copyValueOf(operatorRegistrationDto.getPassword())));
        return new Id(repository.registerOperator(operator).getId());
    }
}
