package me.instajobzz.api.users.registration.repository;

import io.micronaut.spring.tx.annotation.Transactional;
import me.instajobzz.api.users.domain.Operator;
import org.springframework.transaction.annotation.Propagation;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
@Singleton
@Transactional(propagation = Propagation.MANDATORY)
public class RegistrationRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Operator registerOperator(Operator regInfo) {
        entityManager.persist(regInfo);
        return regInfo;
    }
}
