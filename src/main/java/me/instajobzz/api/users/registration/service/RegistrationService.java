package me.instajobzz.api.users.registration.service;

import me.instajobzz.api.users.domain.dto.Id;
import me.instajobzz.api.users.domain.dto.OperatorRegistrationDto;
import me.instajobzz.api.users.invitation.service.InvitationNotFoundException;

public interface RegistrationService {

    Id registerUser(OperatorRegistrationDto userRegistrationInfo, String invitationToken) throws InvitationNotFoundException;
}
