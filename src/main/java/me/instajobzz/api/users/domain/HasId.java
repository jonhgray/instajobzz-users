package me.instajobzz.api.users.domain;

import java.io.Serializable;

public interface HasId<T extends Serializable> {
    T getId();
}
