package me.instajobzz.api.users.domain;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@EqualsAndHashCode
@ToString
public abstract class LiteAggregateRoot<T extends Serializable> implements Serializable, HasId<T> {

    @Id
    @NonNull
    private T id;

    @EqualsAndHashCode.Exclude
    @Column(name = "aud_ts_created", updatable = false)
    private LocalDateTime audTsCreated;

    @EqualsAndHashCode.Exclude
    @Column(name = "aud_ts_last_modified")
    private LocalDateTime audTsUpdated;

    LiteAggregateRoot(T id) {
        initId(id);
    }

    LiteAggregateRoot() { }

    public T getId() {
        return id;
    }

    private void initId(T id) throws IllegalStateException {
        if (this.id != null) {
            throw new IllegalStateException("It is impossible to override identifier value!");
        }
        if (id == null) {
            throw new IllegalArgumentException("'id' argument must not be null!");
        }
        this.id = id;
    }

    public void setId(T id) {
        initId(id);
    }

    @PrePersist
    public void prePersist() {
        audTsCreated = LocalDateTime.now();
        audTsUpdated = audTsCreated;
    }

    @PreUpdate
    public void preUpdate() {
        audTsUpdated = LocalDateTime.now();
    }
}