package me.instajobzz.api.users.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@EqualsAndHashCode
@ToString
public class OperatorRegistrationDto {

    @NotEmpty
    private String name;

    @NotEmpty
    private String surname;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(min = 8)
    private char[] password;

    OperatorRegistrationDto() { }
}
