package me.instajobzz.api.users.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Table(name = "operator")
@Getter
@Setter
public class Operator extends FullAggregateRoot<String> {

    private static final long serialVersionUID = -2336786641947447531L;

    Operator() { }

    @Builder
    public Operator(String id, String name, String surname, String email, String password, Account account) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.account = account;
    }

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Column(name = "password", nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private OperatorRole operatorRole = OperatorRole.ADMIN;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;
}
