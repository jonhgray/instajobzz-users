package me.instajobzz.api.users.domain;

public interface HasConcurrencySafeVersion {
    long concurrencySafeVersion();
}