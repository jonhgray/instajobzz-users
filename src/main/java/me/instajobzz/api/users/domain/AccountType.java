package me.instajobzz.api.users.domain;

public enum AccountType {
    BLOG,
    GROUP,
    COMPANY,
    INDIVIDUAL
}
