package me.instajobzz.api.users.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class OperatorDetails {

    private String id;

    private String name;

    private String surname;

    private String email;

    OperatorDetails() { }
}
