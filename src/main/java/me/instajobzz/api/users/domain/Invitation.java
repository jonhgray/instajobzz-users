package me.instajobzz.api.users.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Table(name = "invitation")
@Getter
@Setter
public class Invitation extends LiteAggregateRoot<String> {

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "expires_at", nullable = false)
    private LocalDateTime expiresAt;

    Invitation() { }

    @Builder
    public Invitation(String id, String email, String token, LocalDateTime expiresAt) {
        super(id);
        this.email = email;
        this.token = token;
        this.expiresAt = expiresAt;
    }
}
