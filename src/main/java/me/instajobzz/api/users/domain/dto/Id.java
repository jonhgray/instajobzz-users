package me.instajobzz.api.users.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Getter
public class Id {
    public Id(String id) {
        this.id = id;
    }

    private String id;
}
