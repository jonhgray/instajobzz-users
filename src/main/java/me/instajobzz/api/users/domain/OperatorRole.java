package me.instajobzz.api.users.domain;

public enum OperatorRole {
    ADMIN,
    MANAGER,
    READER,
    NONE
}
