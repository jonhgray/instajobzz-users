package me.instajobzz.api.users.domain;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String id) {
        super("Пользователь " + id + "не найден");
    }
}
