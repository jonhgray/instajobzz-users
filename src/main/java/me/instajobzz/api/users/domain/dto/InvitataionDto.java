package me.instajobzz.api.users.domain.dto;

import lombok.*;
import me.instajobzz.api.users.domain.LiteAggregateRoot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode
@ToString
@Getter
public class InvitataionDto {

    @NotNull
    @NotEmpty
    @Email
    private String email;

    InvitataionDto() { }

    public InvitataionDto(String email) {
        this.email = email;
    }
}
