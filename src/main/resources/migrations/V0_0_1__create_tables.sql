CREATE TABLE IF NOT EXISTS invitation (
    id                        VARCHAR(36),
    email                     VARCHAR(150) UNIQUE NOT NULL,
    token                     VARCHAR(1024) NOT NULL,
    expires_at                TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    aud_ts_created            TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    aud_ts_last_modified      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    CONSTRAINT pk_invitation_id PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS idx_invitation_email_token ON invitation (email, token);
CREATE INDEX IF NOT EXISTS idx_invitation_token ON invitation (token);

CREATE TABLE IF NOT EXISTS account (
   id                        VARCHAR(36),
   name                      VARCHAR(100) UNIQUE NOT NULL,
   type                      VARCHAR(100) NOT NULL,
   aud_ts_created            TIMESTAMP WITHOUT TIME ZONE NOT NULL,
   aud_ts_created_by         VARCHAR(36),
   aud_ts_last_modified      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
   aud_ts_last_modified_by   VARCHAR(36),
   version                   BIGINT,
   CONSTRAINT pk_account_id PRIMARY KEY (id)
);

COMMENT ON TABLE account
    IS 'Хранение данных об аккаунтах в системе';

CREATE TABLE IF NOT EXISTS profile (
   id                        VARCHAR(36),
   account_id                VARCHAR(36) REFERENCES account(id),
   subjects                  VARCHAR(255)[] DEFAULT '{}',
   social_accounts           jsonb,
   aud_ts_created            TIMESTAMP WITHOUT TIME ZONE NOT NULL,
   aud_ts_created_by         VARCHAR(36),
   aud_ts_last_modified      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
   aud_ts_last_modified_by   VARCHAR(36),
   version                   BIGINT,
   CONSTRAINT pk_user_id PRIMARY KEY (id)
);

COMMENT ON TABLE profile
    IS 'Хранение детальной инфомации об аккаунтах системы';

CREATE TABLE IF NOT EXISTS operator (
  id                        VARCHAR(36),
  name                      VARCHAR(150) NOT NULL,
  surname                   VARCHAR(150) NOT NULL,
  email                     VARCHAR(200) UNIQUE NOT NULL,
  password                  VARCHAR(256) NOT NULL,
  role                      VARCHAR NOT NULL,
  account_id                VARCHAR(26) REFERENCES account(id),
  aud_ts_created            TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  aud_ts_created_by         VARCHAR(36),
  aud_ts_last_modified      TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  aud_ts_last_modified_by   VARCHAR(36),
  version                   BIGINT,
  CONSTRAINT pk_operator_id PRIMARY KEY (id)
);

COMMENT ON TABLE operator
  IS 'Хранение данных об операторах аккаунтов в системе';

CREATE INDEX IF NOT EXISTS idx_operator_email ON operator (email);
CREATE INDEX IF NOT EXISTS idx_fk_operator_account_id ON operator (account_id);

