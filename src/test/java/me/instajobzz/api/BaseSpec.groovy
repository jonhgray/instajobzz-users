package me.instajobzz.api

import me.instajobzz.api.users.configuration.TestConfiguration

import io.micronaut.context.ApplicationContext
import io.micronaut.runtime.server.EmbeddedServer
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

@Testcontainers
abstract class BaseSpec extends Specification {

    @Shared
    @AutoCleanup
    private def postgreSQLContainer = new PostgreSQLContainer("postgres:12-alpine")
            .withDatabaseName("instajobzz_users")
            .withUsername("instajobzz_users")
            .withPassword("secret")

    @Shared
    @AutoCleanup
    protected EmbeddedServer server

    def setupSpec() {
        postgreSQLContainer.start()

        server = ApplicationContext.build()
                .properties(["datasources.default.url": postgreSQLContainer.getJdbcUrl(),
                             "datasources.default.password": postgreSQLContainer.getPassword()])
                .environments("test")
                .packages("me.instajobzz.api.users.configuration.TestConfiguration",
                        "me.instajobzz.api.users.mail.MailSteps",
                        "me.instajobzz.api.users.invitation.InvitationControllerSteps")
                .build()
                .start()
                .getBean(EmbeddedServer)
                .start()
    }
}
