package me.instajobzz.api.users.mail

import org.mockito.ArgumentCaptor

import javax.inject.Inject
import javax.inject.Singleton

import static org.mockito.Mockito.*
import static org.junit.Assert.*
import static org.hamcrest.Matchers.*

@Singleton
class MailSteps {

    @Inject
    private MailService mailService

    private def tokenCaptor = ArgumentCaptor.forClass(String)

    String 'пользователю отправляется письмо с приглашением и валидным токеном на адрес'(String address) {
        verify(mailService, only()).sendInvitationEmail(eq(address), tokenCaptor.capture())
        assertThat(tokenCaptor.getValue(), (not(isEmptyOrNullString())))
        assertThat(tokenCaptor.getValue().length(), greaterThanOrEqualTo(512))
        tokenCaptor.getValue()
    }
}
