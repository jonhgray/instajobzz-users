package me.instajobzz.api.users.configuration

import io.micronaut.context.event.BeanCreatedEvent
import io.micronaut.context.event.BeanCreatedEventListener
import me.instajobzz.api.users.mail.MailService
import org.mockito.Mockito

import javax.inject.Singleton

@Singleton
class TestConfiguration implements BeanCreatedEventListener<MailService> {

    @Override
    MailService onCreated(BeanCreatedEvent<MailService> event) {
        return Mockito.spy(event.getBean())
    }
}