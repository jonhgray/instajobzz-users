package me.instajobzz.api.users.invitation

import io.micronaut.http.HttpStatus
import io.micronaut.http.client.exceptions.HttpClientResponseException
import me.instajobzz.api.users.mail.MailSteps
import groovy.util.logging.Slf4j
import me.instajobzz.api.BaseSpec
import spock.lang.Shared

@Slf4j
class InvitationControllerSpec extends BaseSpec {

    private static final String DEFAULT_EMAIL = "paveltumilovich@gmail.com"

    @Shared
    InvitationControllerSteps приглашение

    @Shared
    MailSteps письмо

    def setupSpec() {
        приглашение = server.applicationContext.getBean(InvitationControllerSteps)
        письмо = server.applicationContext.getBean(MailSteps)
    }

    def "Пользователь должен иметь возможность получить приглашение на сайт по email адресу"() {
        when:
            def статусОтправкиПриглашения = приглашение.'пользователь запрашивает приглашение на адрес'(DEFAULT_EMAIL)
        then:
            статусОтправкиПриглашения == HttpStatus.ACCEPTED
        when:
            def токен = письмо.'пользователю отправляется письмо с приглашением и валидным токеном на адрес'(DEFAULT_EMAIL)
            def статусПриглашения = приглашение.'пользователь запрашивает приглашение по токену'(токен)
        then:
            статусПриглашения == HttpStatus.OK
    }

    def "Пользователь не должен иметь возможность получить приглашение по несуществующему токену"() {
        when:
            def отсутвующийТокен = "not_present_token"
            приглашение.'пользователь запрашивает приглашение по токену'(отсутвующийТокен)
        then:
            HttpClientResponseException e = thrown(HttpClientResponseException)
            e.response.status == HttpStatus.NOT_FOUND
    }

    def "Пользователь не должен иметь возможность получить приглашение по пустому токену"() {
        when:
            def пустойТокен = ""
            приглашение.'пользователь запрашивает приглашение по токену'(пустойТокен)
        then:
            HttpClientResponseException e = thrown(HttpClientResponseException)
            e.response.status == HttpStatus.UNAUTHORIZED
    }
}
