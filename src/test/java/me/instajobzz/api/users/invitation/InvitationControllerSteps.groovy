package me.instajobzz.api.users.invitation

import io.micronaut.context.event.BeanCreatedEvent
import io.micronaut.context.event.BeanCreatedEventListener
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import me.instajobzz.api.users.domain.dto.InvitataionDto

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InvitationControllerSteps {

    @Inject
    @Client("/")
    private RxHttpClient client

    HttpStatus 'пользователь запрашивает приглашение на адрес'(String email) {
        def request = HttpRequest.POST("/invitation", new InvitataionDto(email))
                .contentType(MediaType.APPLICATION_JSON_TYPE)

        client.toBlocking().exchange(request, String).status()
    }

    HttpStatus 'пользователь запрашивает приглашение по токену'(String token) {
        def request = HttpRequest.GET("/invitation/${token}").accept(MediaType.APPLICATION_JSON_TYPE)

        client.toBlocking().exchange(request, String).status()
    }
}
